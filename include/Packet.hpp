#ifndef PACKET_H
#define PACKET_H
#include "array.hpp"
#include "crypto.hpp"	
#include <iostream>

using namespace std;

class Packet {

		private:
	
			array::array *signature, *complete_pack;
			byte tag;

		public:
			array::array *value;

			Packet(byte tag);
			Packet(byte tag,array::array *value);
			~Packet();
	

	array::array *getcomplet_pack();
		array::array* getvalue();
		void setcomplet_pack(array::array* complete_pack);

};
	 
#endif
